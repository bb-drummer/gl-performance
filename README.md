# GitLab SiteSpeed Exporter

# Contributing to the GitLab SiteSpeed Exporter

We welcome contributions and improvements, please see the [contribution guidelines](CONTRIBUTING.md).

# License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.